provider "aws" {
 access_key = "value"
 secret_key = "value"
  region  = "us-east-1"
}





resource "aws_s3_bucket" "myfirstbucket" {
  bucket = "myfirstbucket"
  acl    = "private"

  tags = {
    Name        = "myfirstbucket"
    Environment = "Dev"
  }

   //version 
     versioning {
    enabled = true
  }
       // lifecycle
lifecycle_rule {
   
    enabled = true

    prefix = "log/"

    tags = {
      "rule"      = "log"
      "autoclean" = "true"
    }

    transition {
      days          = 30
      storage_class = "STANDARD_IA" // or "ONEZONE_IA"
    }

    transition {
      days          = 60
      storage_class = "GLACIER"
    }

    expiration {
      days = 90
    }
}
  // logging 
     
  logging {
    target_bucket = aws_s3_bucket.myfirstbucket.id
  }
  //side encryption

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
         
        sse_algorithm     = "AES256"
      }
       }
         }
  
 
}
